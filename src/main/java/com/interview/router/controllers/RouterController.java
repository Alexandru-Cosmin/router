package com.interview.router.controllers;

import com.interview.router.services.RouterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/routing")
public class RouterController {

    @Autowired
    private RouterService routerService;

    @GetMapping("/{origin}/{destination}")
    public ResponseEntity getRoutes(@PathVariable("origin") String origin, @PathVariable("destination") String destination) {
        return routerService.getRoute(origin, destination);
    }
}
