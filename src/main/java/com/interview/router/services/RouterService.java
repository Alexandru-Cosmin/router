package com.interview.router.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.router.domain.Country;
import com.interview.router.domain.Graph;
import com.interview.router.domain.Road;
import com.interview.router.exceptions.DataParseException;
import com.interview.router.exceptions.NoRouteFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class RouterService {

    private static final String DATA_URI = "/mledoze/countries/master/countries.json";

    @Autowired
    private WebClient client;

    private Country[] countries;

    public ResponseEntity getRoute(String origin, String destination){
        Road route;
        try {
            route = computeRoute(origin, destination, countries);
        } catch(NoRouteFoundException ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        } catch(Exception ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(route, HttpStatus.OK);
    }

    private Road computeRoute(String origin, String destination, Country[] countries) throws NoRouteFoundException {
        Graph g = constructGraph(countries);
        g.calculateShortestDistance(getIndexOfCountry(origin, countries), getIndexOfCountry(destination, countries), countries.length);
        Road road = constructRoad(countries, g);
        return road;
    }

    private Road constructRoad(Country[] countries, Graph g) {
        List<String> countriesInRoute = new ArrayList<>();
        for(int i = g.getPath().size()-1 ; i >= 0; i--){
            countriesInRoute.add(countries[g.getPath().get(i)].getCca3());
        }
        return new Road(countriesInRoute);
    }

    private Graph constructGraph(Country[] countries) {
        Graph g = new Graph(countries.length);
        for(int i=0; i<countries.length; i++){
            for(String c : countries[i].getBorders()){
                int index = getIndexOfCountry(c, countries);
                g.addEdge(i, index);
            }
        }
        return g;
    }

    private int getIndexOfCountry(String c, Country[] countries) {
        for(int i=0; i<countries.length; i++){
            if(c.equals(countries[i].getCca3())){
                return i;
            }
        }
        return 0;
    }

    @PostConstruct
    private Country[] mapDataToCountry() throws DataParseException {
        Mono<String> response = client.get()
                .uri(DATA_URI)
                .accept(MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(String.class).log();
        String objects = response.block();
        ObjectMapper mapper = new ObjectMapper();
        try {
            this.countries = mapper.readValue(objects, Country[].class);
        } catch (JsonProcessingException e) {
            throw new DataParseException(e.getMessage());
        }
        return this.countries;
    }
}
