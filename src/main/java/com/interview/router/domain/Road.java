package com.interview.router.domain;

import java.util.List;

public class Road {
    private List<String> route;

    public Road(List<String> route) {
        this.route = route;
    }

    public List<String> getRoute() {
        return route;
    }

    public void setRoute(List<String> route) {
        this.route = route;
    }
}
