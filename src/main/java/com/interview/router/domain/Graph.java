package com.interview.router.domain;

import com.interview.router.exceptions.NoRouteFoundException;

import java.util.ArrayList;
import java.util.LinkedList;

public class Graph {

    private int v;

    private ArrayList<Integer>[] adjList;

    private LinkedList<Integer> path = new LinkedList<Integer>();

    public Graph(int vertices) {

        this.v = vertices;
        initAdjList();
    }

    private void initAdjList() {
        adjList = new ArrayList[v];

        for (int i = 0; i < v; i++) {
            adjList[i] = new ArrayList<>();
        }
    }

    public void addEdge(int u, int v) {
        adjList[u].add(v);
    }

    public void calculateShortestDistance(
            int s, int dest, int v) throws NoRouteFoundException {
        int pred[] = new int[v];
        int dist[] = new int[v];

        if (BFS(s, dest, v, pred, dist) == false) {
            throw new NoRouteFoundException("There is no land route between the two countries!");
        }

        int crawl = dest;
        path.add(crawl);
        while (pred[crawl] != -1) {
            path.add(pred[crawl]);
            crawl = pred[crawl];
        }
    }

    private boolean BFS(int src,
                               int dest, int v, int pred[], int dist[])
    {
        LinkedList<Integer> queue = new LinkedList<Integer>();
        boolean visited[] = new boolean[v];

        for (int i = 0; i < v; i++) {
            visited[i] = false;
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
        }

        visited[src] = true;
        dist[src] = 0;
        queue.add(src);

        while (!queue.isEmpty()) {
            int u = queue.remove();
            for (int i = 0; i < adjList[u].size(); i++) {
                if (visited[adjList[u].get(i)] == false) {
                    visited[adjList[u].get(i)] = true;
                    dist[adjList[u].get(i)] = dist[u] + 1;
                    pred[adjList[u].get(i)] = u;
                    queue.add(adjList[u].get(i));

                    if (adjList[u].get(i) == dest)
                        return true;
                }
            }
        }
        return false;
    }

    public LinkedList<Integer> getPath() {
        return path;
    }

    public void setPath(LinkedList<Integer> path) {
        this.path = path;
    }
}
