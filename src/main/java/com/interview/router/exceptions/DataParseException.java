package com.interview.router.exceptions;

public class DataParseException extends Exception {

    public DataParseException(String message) {
        super("Parsing exception occured, please see cause in following message: " + message);
    }
}
