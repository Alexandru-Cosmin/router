package com.interview.router.exceptions;

public class NoRouteFoundException extends Exception {

    public NoRouteFoundException(String message) {
        super(message);
    }
}
